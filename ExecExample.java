import java.io.IOException;
import java.lang.Runtime;

public class ExecExample {
    public static void main(String[] args) {
        // Get a reference to the Runtime class
        Runtime runtime = Runtime.getRuntime();

        try {
            // Execute a command
            Process process = runtime.exec("ls -l");

            // Read the output of the command
            java.io.InputStream inputStream = process.getInputStream();
            java.util.Scanner scanner = new java.util.Scanner(inputStream).useDelimiter("\\A");
            String commandOutput = scanner.hasNext() ? scanner.next() : "";

            // Print the command output
            System.out.println("Command Output:");
            System.out.println(commandOutput);

            // Wait for the process to complete
            int exitCode = process.waitFor();
            System.out.println("Exit Code: " + exitCode);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
